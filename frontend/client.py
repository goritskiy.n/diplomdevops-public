from flask import Flask, render_template, request
import socket
import os

app = Flask(__name__)
@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>" + "<p>Для ввода данных пройди сюда /login"

@app.route('/api', methods=['post', 'get'])
def hello():
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    IP = socket.gethostbyname(os.getenv("BACKEND_IMAGE", "localhost"))
    PORT = int(os.getenv("BACKEND_PORT", "12333"))
    data = "RUS"
    connection.connect((IP, PORT))
    connection.sendall(data.encode('utf-8'))
    data = connection.recv(1024)
    connection.close()
    print("Ответ " + repr(data.decode('utf-8')))
    return 'Hello, Wooooooorld! Запрос страны ' + data.decode('utf-8')

@app.route('/login/', methods=['post', 'get'])
def login():
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    IP = socket.gethostbyname(os.getenv("BACKEND_IMAGE", "localhost"))
    PORT = int(os.getenv("BACKEND_PORT", "12333"))
    message = ''
    load_message = ''
    get_message = ''
    if request.method == 'POST': # если метод запроса POST, иначе попросить ввести данные
        load_country = request.form.get('load-country') # запрос к данным формы
        get_country = request.form.get('get-country') # запрос к данным формы

        if (bool(load_country)==1 or bool(get_country)==1): # если ввели пустые строки, то попросить ввести данные
            if get_country is None: # если get_country(вывод) пусто, то делаем load_country(загрузку)
                print("Запрос страны из формы: " + str(load_country) + ", обращаетмся к  адресу - " + \
                      str(os.getenv("BACKEND_IMAGE", "localhost")) + ":" + \
                      str(os.getenv("BACKEND_PORT", "12333")))
                connection.connect((IP, PORT))
                send = "L_"+str(request.form.get('load-country'))
                connection.sendall(send.encode('utf-8'))
                data = connection.recv(1024)
                load_message = repr(data.decode('utf-8'))
                print("Ответ от бэкенда - " + repr(data.decode('utf-8')))
                connection.close()
                del load_country
                del get_country
            else:
                print("Запрос на вывод данных из базы по стране - " + \
                      str(get_country) + ", обращаетмся к " + \
                      str(os.getenv("BACKEND_IMAGE", "localhost")) + ":" + \
                      str(os.getenv("BACKEND_PORT", "12333")))
                connection.connect((IP, PORT))
                print("G_"+str(request.form.get('get-country'))+"_"+str(request.form.get('start-date'))+"_"+str(request.form.get('end-date')))
                send = "G_"+str(request.form.get('get-country'))+"_"+str(request.form.get('start-date'))+"_"+str(request.form.get('end-date'))
                connection.sendall(send.encode('utf-8'))
                row = connection.recv(2**20) #1048576
                print(str(row))
                get_message = str(row.decode('utf-8'))
                connection.close()
                del get_country
                del load_country

        else:
            message = "Пожалуйста введите данные 1234"
        return render_template('login.html', message=message, loadmessage=load_message, getmessage=get_message)

    else:
        message = "Пожалуйста введите данные"

    return render_template('login.html', message=message, loadmessage=load_message, getmessage=get_message)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=int(os.getenv("CLIENT_PORT", "5000")))

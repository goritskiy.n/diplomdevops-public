variable "agent_count" {
    default = 3
}

variable "ssh_public_key" {
    default = "C:/Users/User/.ssh/id_rsa.pub"
}

variable "dns_prefix" {
    default = "k8s-ng-diplom"
}

variable cluster_name {
    default = "k8s-ng-diplom"
}

variable resource_group_name {
    default = "ng-diplom-tf"
}

variable location {
    default = "westus2"
}

variable log_analytics_workspace_name {
    default = "LogAnalyticsWorkspace"
}

# refer https://azure.microsoft.com/global-infrastructure/services/?products=monitor for log analytics available regions
variable log_analytics_workspace_location {
    default = "westus2"
}

# refer https://azure.microsoft.com/pricing/details/monitor/ for log analytics pricing
variable log_analytics_workspace_sku {
    default = "PerGB2018"
}

variable gitlab_token {
    default = "glpat-zg8xzpKRodosAMTsTLut"
}

variable gitlab_project {
    default = "32016614"
}

variable administrator_login {
    default = "nikolay_goritskiy"
}

variable administrator_login_password {
    default = "1qazxsw@"
}
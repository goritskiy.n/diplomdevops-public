terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
#      version = "~>3.8.0"
    }
  }
}
provider "azurerm" {
  features {}
}

# Configure the GitLab Provider
provider "gitlab" {
    token = var.gitlab_token
}

#resource "azurerm_resource_group" "k8s" {
#    name     = var.resource_group_name
#    location = var.location
#}

resource "random_id" "log_analytics_workspace_name_suffix" {
    byte_length = 8
}

resource "azurerm_log_analytics_workspace" "test" {
    # The WorkSpace name has to be unique across the whole of azure, not just the current subscription/tenant.
    name                = "${var.log_analytics_workspace_name}-${random_id.log_analytics_workspace_name_suffix.dec}"
    location            = var.log_analytics_workspace_location
    resource_group_name = var.resource_group_name
    sku                 = var.log_analytics_workspace_sku
}

#    tags = {
#        owner = "Nikolai_Goritskii@epam.com"
#    }

resource "azurerm_log_analytics_solution" "test" {
    solution_name         = "ContainerInsights"
    location              = azurerm_log_analytics_workspace.test.location
    resource_group_name   = var.resource_group_name
    workspace_resource_id = azurerm_log_analytics_workspace.test.id
    workspace_name        = azurerm_log_analytics_workspace.test.name

    plan {
        publisher = "Microsoft"
        product   = "OMSGallery/ContainerInsights"
    }

    tags = {
        owner = "Nikolai_Goritskii@epam.com"
    }
}

resource "azurerm_kubernetes_cluster" "k8s" {
    name                = var.cluster_name
    location            = var.location
    resource_group_name = var.resource_group_name
    dns_prefix          = var.dns_prefix

    linux_profile {
        admin_username = "ubuntu"

        ssh_key {
            key_data = file(var.ssh_public_key)
        }
    }

    default_node_pool {
        name            = "agentpool"
        node_count      = var.agent_count
        vm_size         = "Standard_D2_v2"
    }

    identity {
        type = "SystemAssigned"
    }

    addon_profile {
        oms_agent {
        enabled                    = true
        log_analytics_workspace_id = azurerm_log_analytics_workspace.test.id
        }
    }

    network_profile {
        load_balancer_sku = "Standard"
        network_plugin = "kubenet"
    }

    tags = {
        owner = "Nikolai_Goritskii@epam.com"
    }
}

resource "azurerm_sql_server" "azuresql-server" {
  name                         = "ng-azuresql"
  resource_group_name          = var.resource_group_name
  location                     = var.location
  version                      = "12.0"
  administrator_login          = var.administrator_login
  administrator_login_password = var.administrator_login_password

  tags = {
    owner = "Nikolai_Goritskii@epam.com"
  }
}

resource "azurerm_sql_database" "azuresql-db" {
  name                = "ng-diplom-db"
  resource_group_name = var.resource_group_name
  location            = var.location
  server_name         = azurerm_sql_server.azuresql-server.name

  tags = {
    owner = "Nikolai_Goritskii@epam.com"
  }
}

resource "azurerm_sql_firewall_rule" "azuresql-db-rule" {
  name                = "azuresql-db-rule"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_sql_server.azuresql-server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

# Add a variable to the project
resource "gitlab_project_variable" "kube_config" {
    project = var.gitlab_project
    key = "kube_config"
    value = azurerm_kubernetes_cluster.k8s.kube_config_raw
}

resource "gitlab_project_variable" "DB_HOST" {
    project = var.gitlab_project
    key = "DB_HOST"
    value = azurerm_sql_server.azuresql-server.fully_qualified_domain_name
}

resource "gitlab_project_variable" "DB_NAME" {
    project = var.gitlab_project
    key = "DB_NAME"
    value = azurerm_sql_database.azuresql-db.name
}

resource "gitlab_project_variable" "DB_USERNAME" {
    project = var.gitlab_project
    key = "DB_USERNAME"
    value = var.administrator_login
}

resource "gitlab_project_variable" "DB_PASSWORD" {
    project = var.gitlab_project
    key = "DB_PASSWORD"
    value = var.administrator_login_password
}
# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version = "3.8.0"
  hashes = [
    "h1:AAg5ZjtEeLWo/X4vpLe/UKB3R+Cn0kDyIhTkcuv1vpE=",
    "zh:02d977e6803ff336054194e567fcd11d095846346a604017afef98ada0353bc1",
    "zh:07294f6061db86c1ed44e5eae01576d76c585128e334761feb01fca5b866d53f",
    "zh:1cfa4cbbdb9a8a235bb82e7f3968982993750fc5a95bd948b9dfd6de772bff0a",
    "zh:3f9172206c00de224e3e234503681c29f918e2b83dbf5b11c0e791694600bec6",
    "zh:71174040a680be48f65b63fb5571bdd76cd17de830c88b81b6e684b56d77616c",
    "zh:81b1c86b8b59c4f3faed59d461555ce53d86b12f9fc91a2afaee89ca3992f154",
    "zh:a09d2012b4392c01a310713c170db1d7e211190b0e2a27e43ec2f7ca59b6d6c9",
    "zh:a97ed255d3d161313370fc34e83018216dcf198e7a5b285abe31b26bf8b048f3",
    "zh:abfad232ec29ecea620e087d20f1586d0b5b4fc02a6369a6b0c65e80b8de3566",
    "zh:c8814ae34933a082bf68db958ed6ef708321093ce9901a11e2efceaa5b3ed031",
    "zh:e5a52eaada2797fbec6aca19bc7a74cdb40dc1625df999f6a93c34690ea2bc80",
    "zh:f4e1787f9fcab92824dad41486eaa6895a789c2d25f3d3f3126d134bd5edfe90",
    "zh:f90b196e6ff444fe721603765a5be5778bc92d45458009bc77ee6d1b6c32bf67",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version     = "2.86.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:zqkkvTo4pJgqVwUvnvZagtk+PHCnMogdSIr0cSAGaqg=",
    "zh:1568f5eb15da2e4fc31c0c63c56471825f875a5fe3ee2f12be3221e91089f268",
    "zh:227cd0562f950319dc13d2bf57d921f7fada2fe864bff98572db21ed1c1cfc6e",
    "zh:29c04e7315b543abd2f731334a478ff13c346ce4b178b3752d6cd3e9476ba9ae",
    "zh:2da6f960982481bd5f67fa7624d777048d6dcd84489a9df8e4559a55d39280bd",
    "zh:48c94689abcb0d562bd32650adf3b3c7310cba8703a22a7028ceaa378ad27a69",
    "zh:494e7dc72ee869d2cacd36a4155f9c81f940c82b0a68023c36e25ba1e82fbcda",
    "zh:828397e790a3825ffdaec2aadd5dc4d2129b01c14d9307aebfb20c4c2086fb9c",
    "zh:8ca3147360ee049906f1b95a1c3aeddc05cf37b24d51576be5393b7ad926fdcf",
    "zh:a82bf92054203cbf5829ecdb795181ce6984f3550366d49e808840766a4cd8de",
    "zh:de097fdb2295ee1c374c06966f101cdf4c176b8cfac8ca56eb708071c0605e0c",
    "zh:f9cbd78d989aeef10f38cdbab5fac83d8a5a604e338b90455d8e2e7d3226ddcf",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.0"
  hashes = [
    "h1:EPIax4Ftp2SNdB9pUfoSjxoueDoLc/Ck3EUoeX0Dvsg=",
    "zh:2bbb3339f0643b5daa07480ef4397bd23a79963cc364cdfbb4e86354cb7725bc",
    "zh:3cd456047805bf639fbf2c761b1848880ea703a054f76db51852008b11008626",
    "zh:4f251b0eda5bb5e3dc26ea4400dba200018213654b69b4a5f96abee815b4f5ff",
    "zh:7011332745ea061e517fe1319bd6c75054a314155cb2c1199a5b01fe1889a7e2",
    "zh:738ed82858317ccc246691c8b85995bc125ac3b4143043219bd0437adc56c992",
    "zh:7dbe52fac7bb21227acd7529b487511c91f4107db9cc4414f50d04ffc3cab427",
    "zh:a3a9251fb15f93e4cfc1789800fc2d7414bbc18944ad4c5c98f466e6477c42bc",
    "zh:a543ec1a3a8c20635cf374110bd2f87c07374cf2c50617eee2c669b3ceeeaa9f",
    "zh:d9ab41d556a48bd7059f0810cf020500635bfc696c9fc3adab5ea8915c1d886b",
    "zh:d9e13427a7d011dbd654e591b0337e6074eef8c3b9bb11b2e39eaaf257044fd7",
    "zh:f7605bd1437752114baf601bdf6931debe6dc6bfe3006eb7e9bb9080931dca8a",
  ]
}

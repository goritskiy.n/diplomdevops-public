output "client_key" {
    value = azurerm_kubernetes_cluster.k8s.kube_config.0.client_key
}

output "client_certificate" {
    value = azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate
}

output "cluster_ca_certificate" {
    value = azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate
}

output "cluster_username" {
    value = azurerm_kubernetes_cluster.k8s.kube_config.0.username
}

output "cluster_password" {
    value = azurerm_kubernetes_cluster.k8s.kube_config.0.password
}

output "kube_config" {
    value = azurerm_kubernetes_cluster.k8s.kube_config_raw
    sensitive = true
}

output "host" {
    value = azurerm_kubernetes_cluster.k8s.kube_config.0.host
}

output "database_name" {
  description = "Database name of the Azure SQL Database created."
  value       = azurerm_sql_database.azuresql-db.name
}

output "sql_server_name" {
  description = "Server name of the Azure SQL Database created."
  value       = azurerm_sql_server.azuresql-server.name
}

output "sql_server_location" {
  description = "Location of the Azure SQL Database created."
  value       = azurerm_sql_server.azuresql-server.location
}

output "sql_server_fqdn" {
  description = "Fully Qualified Domain Name (FQDN) of the Azure SQL Database created."
  value       = azurerm_sql_server.azuresql-server.fully_qualified_domain_name
}

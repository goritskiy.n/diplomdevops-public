import requests
import pyodbc
import socket
import os
from datetime import timedelta, datetime
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# Данные для подключения к базе данных
server = os.getenv("DB_HOST", "ng-diplom-dbsrv.database.windows.net")
database = os.getenv("DB_NAME", "ng-diplom-db")
username = os.getenv("DB_USERNAME", "goritskiy")
password = os.getenv("DB_PASSWORD", "1")
driver = os.getenv("DB_DRIVER", "{ODBC Driver 17 for SQL Server}")

# Подключемся к базе, удаляем старую и создаем новую таблицу
try:
    with pyodbc.connect(
            'DRIVER=' + driver + ';SERVER=tcp:' + server + ';PORT=1433;DATABASE=' + database + ';UID=' + username + ';PWD='
            + password) as conn:
        with conn.cursor() as cursor:
            #cursor.execute("DROP TABLE IF EXISTS dbo.DATA")
            cursor.execute(
                "CREATE TABLE DATA(date_value DATE, country_code VARCHAR(100), confirmed INT, deaths INT, stringency FLOAT, stringency_actual FLOAT)")
            #cursor.execute("SELECT * FROM dbo.DATA")
except pyodbc.ProgrammingError:
    print("Предупреждение: База данных уже существует. ")
# Стратегия обработки ошибок при URL запросах
def requests_retry_session(
        retries=3,
        backoff_factor=0.3,
        status_forcelist=(500, 502, 504),
        session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session

# Открываем сокеты и ждем данные
listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0)
listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
IP = "0.0.0.0"
PORT = int(os.getenv("BACKEND_PORT", "12333"))
listener.bind((IP, PORT))
listener.listen(10)
print(IP)

# Счетчик Except
except_KeyError = 0

# Формат даты
date_format = "%Y-%m-%d" # sonarcloud code smell duplicating

while True:  # цикл бесконечного прослушивания
    connection, address = listener.accept()
    print('Присоединился к сокету: ', address)

    while True:  # цикл постоянной обработки данных полученных от клиента
        client_data = connection.recv(1024).decode('utf-8')
        if not client_data:
            break
        print("Данные от клиента: " + client_data)
        #connection.sendall(client_data.encode('utf-8'))
        country = client_data.split('_')[1]
        method = client_data.split('_')[0]
        now = datetime(2021, 1, 20)  # исправить на now = datetime.now(), в прод версии, в тест - now = datetime(2021, 1, 20)
        one_days = timedelta(1)
        now = now - one_days
        date = datetime(2021, 1, 1)
        if str(method)=="L":
            print("Country = " + country)
            print("Method = " + method)
            while True:  # Цикл запроса всех дней в году до текущего(минус один)
                try:
                    print("Текущий запрос: Страна: " + country + " " + "Дата: " + str(date.strftime(date_format)))
                    response = requests_retry_session().get(
                        "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/actions/" + country + "\/" + str(
                            date.strftime(date_format)), timeout=10).json()
                    if response['stringencyData']['stringency'] is None:
                        stringency_none = 0
                    else:
                        stringency_none = str(response['stringencyData']['stringency'])
                    print("Код страны: " + str(response['stringencyData']['country_code']) + "/Дата: " + str(
                        response['stringencyData']['date_value']) + "/Потдверждено случаев: " + str(
                        response['stringencyData']['confirmed']) + "/Смертей: " + str(
                        response['stringencyData']['deaths']) + "/Точность: " + str(
                        stringency_none) + "/Фактическая точность: " + str(stringency_none))

                    # Отправляем данные в базу
                    with conn.cursor() as cursor:
                        cursor.execute(
                                "INSERT INTO DATA VALUES (\'" + str(response['stringencyData']['date_value']) + "\', \'" + str(
                                    response['stringencyData']['country_code']) + "\', \'" + str(
                                    response['stringencyData']['confirmed']) + "\', \'" + str(
                                    response['stringencyData']['deaths']) + "\', \'" + str(stringency_none) + "\', \'" + str(
                                    stringency_none) + "\')")
                    date = date + one_days
                    print(
                        "Условие, это: " + date.strftime(date_format) + " должно быть равно этому: " + now.strftime(date_format))
                    if date.strftime(date_format) == now.strftime(date_format):
                        data = "Загрузка данных в базу, успешно завершена!"
                        connection.sendall(data.encode('utf-8'))
                        print(data)
                        break
                except KeyError:
                    print("KeyError!")
                    except_KeyError += 1
                    date = date + one_days
                    if date.strftime(date_format) == now.strftime(date_format):
                        data = "Загрузка данных в базу, завершена c ошибками!"
                        connection.sendall(data.encode('utf-8'))
                        print(data)
                        break
                    if except_KeyError == 30:
                        data = "Данные введены не верно, нет данных по этой стране - " + str(country)
                        connection.sendall(data.encode('utf-8'))
                        print(data)
                        except_KeyError = 0
                        break
                    continue
                except ConnectionError:
                    if date.strftime(date_format) == now.strftime(date_format):
                        data = "Загрузка данных в базу, завершена c ошибками!"
                        connection.sendall(data.encode('utf-8'))
                        print(data)
                        break
                    print("ConnectionError!")
                    continue

        else: # получаем данные из базы
            start_date = client_data.split('_')[2]
            end_date = client_data.split('_')[3]
            print("Country = " + country)
            print("Method = " + method)
            print("Start date = " + start_date)
            print("End date = " + end_date)
            with conn.cursor() as cursor:
                cursor.execute("SELECT DISTINCT date_value, country_code, confirmed, deaths, stringency, "
                               "stringency_actual FROM[dbo].[DATA] WHERE country_code = \'" + str(country) +
                               "\' AND date_value BETWEEN \'" + str(start_date) + "\' AND \'" + str(end_date) +
                               "\' ORDER BY deaths ASC")
                table = "<tr><th>Дата1</th><th>Страна</th><th>\
                            Подтверждено</th><th>Смертей</th><th>\
                            Точность</th><th>Актуальная_точность</th></tr>"

                while 1:
                    row = cursor.fetchone()
                    if not row:
                        print(table)
                        connection.sendall(table.encode('utf-8'))
                        break
                    table_co = "</td><td>" # sonarcloud code smell duplicating
                    table = table + "<tr><td>" + str(row.date_value) + table_co + str(row.country_code) + table_co + \
                            str(row.confirmed) + table_co + str(row.deaths) + table_co + \
                            str(row.stringency) + table_co + str(row.stringency_actual) + "</td></tr>"
                    #print(table)
                    #print(str(row.date_value) + " " + str(row.country_code) + " " + str(row.confirmed) + " " + str(row.deaths) + " " + str(row.stringency) + " " + str(row.stringency_actual))
    connection.close()